![logo.png](https://bitbucket.org/repo/Rj698G/images/2273056683-logo.png)
# JUnit Extras

This project is a collection of extensions of JUnit.  For now, included extensions are:
  
* RegexSuite, which in addition to the list of classes in the "official" suite implementation allows to collect test classes via regular expressions matched against qualified class names.
  
* CSVReader provides methods for reading CSV-files and return objects  of type List\<Object\[\]\>, ideal for the creation of parametrised test classes. 
  Filters are provided for String, Double, Integer, Long, Boolean, and Dates
  
* Additional matchers to work with assertThat(): 
  For examples, see the javadoc and the sources for the tests.

# Usage

Add the following dependency to your project:
~~~ .xml
	 <groupId>org.bitbucket.gthimm.junitextras</groupId>
	 <artifactId>junit-extras</artifactId>
	 <version>1.0.0</version>
	 <scope>test></scope>
~~~	
See the java doc for explanations on how to use the extras or
look at the class ProductsAndSumsDemo and CSVReaderDemo for examples.

## Logging
Depending on your project setup, you may have to add a dependency 
~~~ .xml
     <groupId>org.slf4j</groupId>
     <artifactId>slf4j-simple</artifactId>
     <version>1.7.10</version>
     <scope>test</scope>
~~~
in order to see the logs by the junit extra.

## Bug, enhancements, etc

Via ticket on https://bitbucket.org/gthimm/junit-extras/