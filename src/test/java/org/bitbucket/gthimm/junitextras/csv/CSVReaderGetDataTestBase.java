/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;

public abstract class CSVReaderGetDataTestBase {
    /**
     * the contents of the file test.csv as strings.
     */
    static final List<Object[]> TEST_CSV_AS_STRINGS = Arrays.asList(new Object[][] {
            { "test", "TRUE" }, { "more", "false" }, { "willy", "ignore" }, { "billy", "true" } });

    /**
     * the contents of the file test2.csv as the test should read the data.
     */
    static final List<Object[]> TEST2_CSV_FILE_CONTENT = Arrays.asList(new Object[][] {
            { "A", "1", "1.1" }, { "B", "2", "2.2" }, { "C", "3", "3.3" } });
    /**
     * the fixture: the data as read from a file. Initialise in an
     * implementation @BeforeClass
     */
    static List<Object[]> fixture = null;
    /**
     * the data as the actual test would expect it to be. Initialise in an
     * implementation @BeforeClass
     */
    static List<Object[]> expected = null;

    /**
     * ensure that something was read.
     */
    @Test
    public void testFixtureNotNullNotEmpty() {
        assertThat("fixture is null", fixture, notNullValue());
        assertThat("fixture is empty", fixture.size(), not(equalTo(0)));
    }

    /**
     * test that the contents of the fixture and the expected values are the
     * same.
     */
    @Test
    public void testContent() {
        assertThat(expected, notNullValue());
        assertThat("content  differs", fixture.toArray(), equalTo(expected.toArray()));
    }

    @AfterClass
    public static void cleanup() {
        // this is needed as the list and expected are read in a @BeforeClass
        // methods.
        fixture = null;
        expected = null;
    }

}
