/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.junit.Test;

public class StringFilterTest {
    List<String> fixture = Arrays.asList(new String[] { "one", "two", "three" });

    /**
     * test that the right column is returned by the {@link StringColumnFilter}
     *
     * @throws CSVIgnoreRow
     */
    @Test
    public void testApply() throws CSVIgnoreRow {
        for (int i = 0; i < fixture.size(); i++) {
            StringFilter sc = new StringFilter(i + 1);
            assertEquals(sc.apply(fixture), fixture.get(i));
        }
    }

    /**
     * test that an Exception is thrown if the column is illegal
     *
     * @throws CSVIgnoreRow
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testExceptionIfColIllegal() throws CSVIgnoreRow {
        ColumnFilter sc = new StringFilter(1000);
        sc.apply(fixture);
    }
}
