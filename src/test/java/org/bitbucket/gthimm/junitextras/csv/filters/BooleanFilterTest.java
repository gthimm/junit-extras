/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.CSVReaderGetDataTestBase;
import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.bitbucket.gthimm.junitextras.csv.filters.BooleanFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilterImpl;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.junit.Test;

/**
 * tests for the BooleanFilter
 */
public class BooleanFilterTest {
    /**
     * strings which can be translated into boolean values
     */
    private final List<String> fixture = Arrays.asList(new String[] { "true", "false", "t", "f",
            "TRUE", "F" });
    /**
     * the i-th element of the fixture should be translated into the i-th value
     * of the array expected.
     */
    private final Boolean[] expected = new Boolean[] { TRUE, FALSE, TRUE, FALSE, TRUE, FALSE };

    /**
     * test that the right column is returned by the {@link StringColumnFilter}
     *
     * @throws CSVIgnoreRow
     *             should not occur.
     */
    @Test
    public void testApply() throws CSVIgnoreRow {
        for (int i = 0; i < fixture.size(); i++) {
            final ColumnFilter sc = new BooleanFilter(i + 1);
            assertThat("for input " + i + " " + fixture.get(i), (Boolean) sc.apply(fixture),
                    equalTo(expected[i]));
        }
    }

    /**
     * test that an InvalidCSVFormatException is thrown if the column is illegal
     *
     * @throws CSVIgnoreRow
     *             should not occur
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testExceptionIfColIllegal() throws CSVIgnoreRow {
        final BooleanFilter sc = new BooleanFilter(1000);
        sc.apply(fixture);
    }

    /**
     * test that CSVIgnoreRow is thrown if the string "ignore" occurs in the
     * input
     *
     * @throws CSVIgnoreRow
     *             must occur here.
     */
    @Test(expected = CSVIgnoreRow.class)
    public void testIgnoreRow() throws CSVIgnoreRow {
        final ColumnFilter sc = new BooleanFilter(1);
        sc.apply(Arrays.asList("ignore"));
    }

    /**
     * test that {@link InvalidCSVFormatException} is thrown if the value to be
     * parsed is "ignore" and the filter has the .
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testInvalidCSVForUnknownDataIfNonIgnoring() throws CSVIgnoreRow {
        final ColumnFilter sc = new BooleanFilter(1, false);
        sc.apply(Arrays.asList(ColumnFilterImpl.IGNORE_TAG));
    }

    /**
     * test that {@link CSVIgnoreRow} is thrown if the value to be parsed is
     * "ignore" and the filter has the .
     */
    @Test(expected = CSVIgnoreRow.class)
    public void testCSVForCSVIgnoreRow() throws CSVIgnoreRow {
        final ColumnFilter colFilter = new BooleanFilter(1, true);
        colFilter.apply(Arrays.asList(ColumnFilterImpl.IGNORE_TAG));
    }

    /**
     * test that {@link InvalidCSVFormatException} is thrown if some arbitrary
     * string is parsed by the filter.
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testInvalidCSVForUnknownData() throws CSVIgnoreRow {
        final ColumnFilter sc = new BooleanFilter(1);
        sc.apply(Arrays.asList("billy"));
    }

    /**
     * Test that the filter works on a data file. If a field to be converted to
     * boolean contains the string "ignore", getData does correctly ignore it.
     *
     * @throws IOException
     */
    @Test
    public void testGetDataIgnoresIgnore() throws IOException {
        final StringFilter stringFilter = new StringFilter(1);
        final BooleanFilter booleanFilter = new BooleanFilter(2);
        final List<Object[]> list = CSVReader.getData(
                CSVReaderGetDataTestBase.class.getResource("test.csv"), ",", stringFilter,
                booleanFilter);
        assertThat("list has wrong size", list.size(), equalTo(3));
        assertThat(list.get(0)[0], equalTo((Object) "test"));
        assertThat(list.get(0)[1], equalTo((Object) TRUE));
        assertThat(list.get(1)[0], equalTo((Object) "more"));
        assertThat(list.get(1)[1], equalTo((Object) FALSE));
        assertThat(list.get(2)[0], equalTo((Object) "billy"));
        assertThat(list.get(2)[1], equalTo((Object) TRUE));
    }

}
