/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.DateFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * test the {@link DateFilter} for a German default locale
 *
 */
@RunWith(Parameterized.class)
public class DateFilterTest {

    private static final List<String> DATE_STRINGS = Arrays.asList(new String[] { "01-Dez-2010",
            "2010-01-17 18:33:51", "2010-1-7", "201003" });
    private final Integer column;
    private final SimpleDateFormat dateFormat;
    private final Long expected;

    /**
     *
     * @return a list of fixtures with the index of the date in
     *         {@link #DATE_STRINGS}, a date format, and the time in
     *         milliseconds to which the date should be translated.
     */
    @Parameters
    public static List<Object[]> indexAndFormats() {
        Locale.setDefault(Locale.GERMAN);
        Object[][] data = new Object[][] {
                { 0, new SimpleDateFormat("dd-MMM-yyyy"), 1291161600000L - 3600000L },
                { 1, new SimpleDateFormat("yyyy-mm-dd HH:mm:ss"), 1263753231000L - 3600000L },
                { 2, new SimpleDateFormat("yyyy-mm-dd"), 1262822460L * 1000L - 3600000L },
                { 3, new SimpleDateFormat("yyyyMM"), 1267401600L * 1000L - 3600000L } };
        return Arrays.asList(data);
    }

    public DateFilterTest(Integer column, SimpleDateFormat dateFormat, final Long expected) {
        this.column = column;
        this.dateFormat = dateFormat;
        this.expected = expected;
    }

    /**
     * test that the right column is returned by the {@link StringColumnFilter}
     *
     * @throws Exception
     */
    @Test
    public void testApply() throws Exception {
        DateFilter sc = new DateFilter(column + 1, dateFormat);
        try {
            // make sure there is a date.
            assertThat("for input " + column + " " + DATE_STRINGS.get(column) + " format="
                    + dateFormat.toString(), sc.apply(DATE_STRINGS), notNullValue());
            // check the value
            assertThat("for input " + column + " " + DATE_STRINGS.get(column) + " diff="
                    + (expected - sc.apply(DATE_STRINGS).getTime()), sc.apply(DATE_STRINGS)
                    .getTime(), equalTo(expected));
        } catch (InvalidCSVFormatException e) {
            fail("Exception " + e.getClass() + "for input " + column + ": "
                    + DATE_STRINGS.get(column) + " format=" + dateFormat.toString());
        }
    }

    /**
     * test that an Exception is thrown if the column is illegal
     *
     * @throws CSVIgnoreRow
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testExceptionIfColIllegal() throws CSVIgnoreRow {
        ColumnFilter sc = new DateFilter(1000, dateFormat);
        sc.apply(DATE_STRINGS);
    }

    /**
     * test that {@link InvalidCSVFormatException} is thrown if the value to be
     * parsed is "ignore" and the filter has the .
     *
     * @throws InvalidCSVFormatException
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testInvalidCSVForUnknownDataIfNonIgnoring() throws CSVIgnoreRow {
        ColumnFilter sc = new DateFilter(1, new SimpleDateFormat(), false);
        sc.apply(Arrays.asList("ignore"));
    }

    /**
     * test that CSVIgnoreRow is thrown if the string "ignore" occurs in the
     * input
     *
     * @throws CSVIgnoreRow
     *             must occur here.
     */
    @Test(expected = CSVIgnoreRow.class)
    public void testIgnoreRow() throws CSVIgnoreRow {
        ColumnFilter sc = new DateFilter(1, new SimpleDateFormat());
        sc.apply(Arrays.asList("ignore"));
    }
}
