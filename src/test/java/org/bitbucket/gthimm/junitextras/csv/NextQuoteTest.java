/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.junit.Test;

/**
 * test that {@link CSVReader#nextDoubleQuote(String, int)} ignores
 * double quotes as column separators
 */
public class NextQuoteTest {

    @Test
    public void testNextDoubleQuote() throws Exception {
        // ///////// 0 1 2 34 56789 0
        String s = "\"\"\"\"a\"bdef\"";
        assertThat(CSVReader.nextDoubleQuote(s, 6), equalTo(10));
        assertThat(CSVReader.nextDoubleQuote(s, 3), equalTo(3));
        assertThat(CSVReader.nextDoubleQuote(s, 2), equalTo(5));
        assertThat(CSVReader.nextDoubleQuote(s, 5), equalTo(5));
        assertThat(CSVReader.nextDoubleQuote(s, 4), equalTo(5));
    }

    @Test(expected = InvalidCSVFormatException.class)
    public void testNextSingleQuoteException1() {
        CSVReader.nextDoubleQuote("\"a b c", 1);
    }

    @Test
    public void testnextSingleQuote() {
        assertThat(CSVReader.nextDoubleQuote("\"\"\"\"", 1), equalTo(3));
    }

    @Test(expected = InvalidCSVFormatException.class)
    public void testnextSingleQuoteException3() {
        CSVReader.nextDoubleQuote("\"\"\"\"", 0);
    }

}
