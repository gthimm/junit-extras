/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import java.io.IOException;
import java.util.Arrays;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.filters.BooleanFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.junit.BeforeClass;

public class GetDataWithFilterTest extends CSVReaderGetDataTestBase {
    /**
     * initialise the fixture and from the file test.csv and the expected
     * values.
     */
    @BeforeClass
    public static void setList() throws IOException {
        fixture = CSVReader.getData(GetDataWithFilterTest.class.getResource("test.csv"),
                ",", new StringFilter(1), new BooleanFilter(2));
        expected = Arrays.asList(new Object[][] { { "test", Boolean.TRUE },
                { "more", Boolean.FALSE }, { "billy", Boolean.TRUE } });
    }

}
