/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import java.io.IOException;
import java.util.ArrayList;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.junit.BeforeClass;

public class GetDataWithoutHeaderTest extends CSVReaderGetDataTestBase {

    /**
     * Initialise the fixture from the CSV file
     */
    @BeforeClass
    public static void setList() throws IOException {
        fixture = CSVReader.getData(
                CSVReaderGetDataTestBase.class.getResource("test.csv"), ",", true,
                new StringFilter(1), new StringFilter(2, false));
        // remove the first entry from the default test result
        expected = new ArrayList<Object[]>(TEST_CSV_AS_STRINGS);
        expected.remove(0);
    }

}
