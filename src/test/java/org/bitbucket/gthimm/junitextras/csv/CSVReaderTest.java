/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * test for some examples that the columns are separated correctly.
 */
@RunWith(Parameterized.class)
public class CSVReaderTest {

    private final String fixture;
    private final String[] expected;

    /**
     *
     * @return a list of fixtures and expected values: a string to be split and
     *         the expected result from splitting the string.
     */
    @Parameters
    public static List<Object[]> data() {
        Object[][] data = new Object[][] { { "a b c; d e f", new String[] { "a b c", "d e f" } },
                { "\"a \"; d ;	e	", new String[] { "a ", "d", "e" } },
                { "a b c; d e f", new String[] { "a b c", "d e f" } },
                { "x;y;z;", new String[] { "x", "y", "z", "" } },
                { "\"a;b\"", new String[] { "a;b" } },
                { "\"a;b\";de", new String[] { "a;b", "de" } }, { "", new String[] { "" } },
                { "\"\"", new String[] { "\"" } }, { "a\"\"b", new String[] { "a\"b" } } };
        return Arrays.asList(data);
    }

    public CSVReaderTest(String input, String[] expected) {
        this.expected = expected;
        this.fixture = input;
    }

    @Test
    public void test() {
        try {
            assertThat("input=>>>" + fixture + "<<<",
                    CSVReader.splitLineToStringArray(fixture, ";"), equalTo(expected));
        } catch (InvalidCSVFormatException e) {
            fail("InvalidCSVFormatException for input=>>>" + fixture + "<<<");
        }
    }
}
