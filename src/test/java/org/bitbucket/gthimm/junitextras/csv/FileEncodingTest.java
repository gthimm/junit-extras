/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.nio.charset.Charset;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.junit.Test;

public class FileEncodingTest {

    /**
     * test that the encoding of a file is dealt with correctly
     **/
    @Test
    public void testUTF8GetDataSeparator() throws Exception {
        CSVReader.setUsedFileEncoding(Charset.forName("utf8"));
        List<Object[]> data = CSVReader.getData(getClass().getResource("utf8.csv"), ",");
        assertThat((String) data.get(0)[0], equalTo("€ÄÖÜäöü"));
    }

    /**
     * test that the encoding of a file is dealt with correctly
     **/
    @Test
    public void testISO88591GetDataSeparator() throws Exception {
        CSVReader.setUsedFileEncoding(Charset.forName("ISO-8859-1"));
        List<Object[]> data = CSVReader.getData(getClass().getResource("iso-8859-1.csv"),
                ",");
        assertThat((String) data.get(0)[0], equalTo("ÄÖÜäöü"));

    }

}
