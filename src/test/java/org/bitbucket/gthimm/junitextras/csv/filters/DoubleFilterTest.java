/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.DoubleFilter;
import org.junit.Test;

/**
 * tests for the DoubleFilter
 */
public class DoubleFilterTest {
    /**
     * some strings to be translated into doubles.
     */
    private final List<String> fixture = Arrays.asList(new String[] { "1", "2", "3.0" });
    /** The expected values. **/
    private final Double[] expected = new Double[] { 1.0, 2.0, 3.0 };

    /**
     * test that the right column is returned by the {@link StringColumnFilter}
     *
     * @throws CSVIgnoreRow
     *             should not occur.
     */
    @Test
    public void testApply() throws CSVIgnoreRow {
        for (int i = 0; i < fixture.size(); i++) {
            ColumnFilter sc = new DoubleFilter(i + 1);
            assertThat("for input " + i + " " + fixture.get(i), (Double) sc.apply(fixture),
                    equalTo(expected[i]));
        }
    }

    /**
     * test that an Exception is thrown if the column is illegal
     *
     * @throws CSVIgnoreRow
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testExceptionIfColIllegal() throws CSVIgnoreRow {
        ColumnFilter sc = new DoubleFilter(1000);
        sc.apply(fixture);
    }

    /**
     * test that a junit failure rises {@link CSVIgnoreRow}
     *
     * @throws CSVIgnoreRow
     */
    @Test(expected = InvalidCSVFormatException.class)
    public void testInvalidCSVForUnknownData() throws CSVIgnoreRow {
        ColumnFilter sc = new DoubleFilter(1);
        sc.apply(Arrays.asList(new String[] { "billy" }));
    }

}
