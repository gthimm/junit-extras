/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.suite;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bitbucket.gthimm.junitextras.suite.RegexSuite.SuiteClasses;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

/**
 * Test the include/exclude feature of {@link RegexSuite}
 */
public class RegexSuiteTest {
    /**
     * a regular expression which is used to select some classes (used in
     * fixtures). These classes are from both, the test and the main sources!
     */
    static final String SUITE_REGEX = "org\\.bitbucket\\.gthimm\\.junitextras\\..*RegexSuite.*";
    /**
     * the package name of this very class
     */
    static final String REGEX_SUITE_PACKAGE = RegexSuiteTest.class.getPackage().getName();
    /**
     * the class name of this very class
     */
    static final String REGEX_SUITE_TEST_CLASS_NAME = RegexSuiteTest.class.getCanonicalName();
    /**
     * the file name of the compiled class file.
     */
    private static File classFile = new File("target/test-classes/"
            + REGEX_SUITE_TEST_CLASS_NAME.replaceAll("\\.",
                    Matcher.quoteReplacement(File.separator)) + ".class");

    /**
     * check that the class file exists.
     */
    @BeforeClass
    public static void checkSetup() {
        assertTrue("cannot find file " + classFile.getAbsolutePath(), classFile.exists());
    }

    /**
     * The fixture for {@link #testGetAnnotatedClasses()}.
     */
    @RunWith(RegexSuite.class)
    @SuiteClasses(include = SUITE_REGEX, exclude = "")
    public class TestSuiteTest1 {
        // nothing needed here
    }

    /**
     * The fixture for {@link #testGetAnnotatedClasses()}. same as above,
     * without inner classes
     */
    @RunWith(RegexSuite.class)
    @SuiteClasses(include = SUITE_REGEX, exclude = ".*\\$.*")
    public class TestSuiteTest2 {
        // nothing needed here
    }

    /**
     * The fixture for {@link #testGetAnnotatedClasses()}.
     */
    @RunWith(RegexSuite.class)
    @SuiteClasses(RegexSuiteTest.class)
    public class TestSuiteTest3 {
        // nothing needed here
    }

    /**
     * test whether the suite of TestSuiteTest1 is extended to 6 classes and
     * contains the "RegexSuite" class
     *
     * @throws InitializationError
     * @throws ClassNotFoundException
     */
    @Test
    public void testGetAnnotatedClasses1() throws InitializationError, ClassNotFoundException {
        Class<?> suite = ClassLoader.getSystemClassLoader().loadClass(
                REGEX_SUITE_TEST_CLASS_NAME + "$TestSuiteTest1");
        List<Class<?>> set = Arrays.asList(RegexSuite.getAnnotatedClasses(suite));
        assertEquals("found " + set, 6, set.size());
        assertThat(
                set,
                hasItems(SuiteClasses.class, TestSuiteTest3.class, TestSuiteTest1.class,
                        TestSuiteTest2.class, RegexSuiteTest.class, RegexSuite.class));
    }

    /**
     * test whether the suite of TestSuiteTest2 is extended to 2 classes and
     * contains the "RegexSuite" class
     */
    @Test
    public void testGetAnnotatedClasses2() throws InitializationError, ClassNotFoundException {
        Class<?> suite = ClassLoader.getSystemClassLoader().loadClass(
                REGEX_SUITE_TEST_CLASS_NAME + "$TestSuiteTest2");
        List<Class<?>> set = Arrays.asList(RegexSuite.getAnnotatedClasses(suite));
        assertEquals("found " + set, 2, set.size());
        assertThat(set, hasItems(RegexSuiteTest.class, RegexSuite.class));
    }

    /**
     * test whether the suite of TestSuiteTest3 is extended to class
     * RegexSuiteTest
     */
    @Test
    public void testGetAnnotatedClasses3() throws InitializationError, ClassNotFoundException {
        Class<?> suite = ClassLoader.getSystemClassLoader().loadClass(
                REGEX_SUITE_TEST_CLASS_NAME + "$TestSuiteTest3");
        List<Class<?>> set = Arrays.asList(RegexSuite.getAnnotatedClasses(suite));
        assertEquals("found " + set, 1, set.size());
        assertTrue("missing " + RegexSuiteTest.class, set.contains(RegexSuiteTest.class));
    }

    /**
     * a few test cases for
     * {@link RegexSuite#isTestClass(File, String, Pattern, Pattern)} to return
     * the class name or null, depending on whether the include/exclude pattern
     * are accordingly constructed.
     */
    @Test
    public void testIsTestClass() {
        assertEquals(
                REGEX_SUITE_TEST_CLASS_NAME,
                RegexSuite.isTestClass(classFile, REGEX_SUITE_PACKAGE,
                        Pattern.compile(".*RegexSuite.*"), Pattern.compile("")));
        assertEquals(
                REGEX_SUITE_TEST_CLASS_NAME,
                RegexSuite.isTestClass(classFile, REGEX_SUITE_PACKAGE,
                        Pattern.compile(".*RegexSuite.*"), Pattern.compile("nothing")));
        assertNull(RegexSuite.isTestClass(classFile, REGEX_SUITE_PACKAGE,
                Pattern.compile(".*RegexSuite.*"), Pattern.compile(".*Test")));
        assertNull(RegexSuite.isTestClass(classFile, REGEX_SUITE_PACKAGE,
                Pattern.compile(".*XXXX.*"), Pattern.compile("nothing")));
    }

    /**
     * if a non-existing class file is tested to be a test, the return value of
     * {@link RegexSuite#isTestClass(File, String, Pattern, Pattern)} must
     * return null.
     */
    @Test
    public void testLoadingFromInexistingClass() {
        assertNull(RegexSuite.isTestClass(new File("/no/where"), REGEX_SUITE_PACKAGE,
                Pattern.compile(".*RegexSuite.*"), Pattern.compile("")));
    }
}
