/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras;

import org.bitbucket.gthimm.junitextras.suite.RegexSuite;
import org.bitbucket.gthimm.junitextras.suite.RegexSuite.SuiteClasses;
import org.junit.runner.RunWith;

/**
 * A suite containing all demos (written as junit cases). Some "tests" fail
 * intentionally
 */
@RunWith(RegexSuite.class)
@SuiteClasses(include = "org\\.bitbucket\\.gthimm\\.junitextras\\..*Demo$")
public class AllDemos {
    /**
     * The runner does all the work
     */
}
