/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.demo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilterImpl;
import org.bitbucket.gthimm.junitextras.csv.filters.IntegerFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * A demo for the CSV - parameterised runner with customised filters. The demo
 * uses the file sum_and_product.csv, containing 3 numbers each row, where the
 * 3rd number is compared with the sum and the product of the first two numbers.
 *
 * NOT ALL TESTS ARE WRITTEN TO SUCCEED !!!!
 *
 */
@RunWith(Parameterized.class)
public class ProductsAndSumsDemo {

    private static final String SUM_AND_PRODUCT_CSV = "sum_and_product.csv";

    /**
     * a shell to hold a sum and a product.
     */
    private static class SumAndProduct {
        private final int sum;
        private final int product;

        public SumAndProduct(int sum, int product) {
            this.sum = sum;
            this.product = product;
        }

        @Override
        public String toString() {
            return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

        public int getProduct() {
            return product;
        }

        public int getSum() {
            return sum;
        }
    }

    /**
     * This filter returns the sum and product of two columns parsed as integer
     * numbers For simplicity, the filter assumes that the values are in the
     * first two columns.
     */
    private static final class SumAndProductFilter implements ColumnFilter<SumAndProduct> {

        static final SumAndProductFilter INSTANCE = new SumAndProductFilter();

        private SumAndProductFilter() {
        }

        public SumAndProduct apply(List<String> row) throws CSVIgnoreRow {
            int a = Integer.parseInt(row.get(0));
            int b = Integer.parseInt(row.get(1));
            return new SumAndProduct(a + b, a * b);
        }
    }

    /**
     * this filter does the same thing as the {@link IntegerFilter}, but, if
     * some error occurs (absence of the column, non-integer string values), the
     * row is ignored.
     */
    private static class ErrorIgnoringIntegerFilter extends ColumnFilterImpl<Integer> {

        public ErrorIgnoringIntegerFilter(int i) {
            super(i);
        }

        @Override
        public Integer apply(List<String> row) throws CSVIgnoreRow {
            try {
                String value = getColumn(row);
                return Integer.parseInt(value);
            } catch (Throwable e) {
                // just ignore the row!
                throw new CSVIgnoreRow();
            }
        }
    }

    /**
     * fixture: the first column from the csv file
     */
    private final int valueA;
    /**
     * fixture: the value in the 2nd column of the csv file
     */
    private final int valueB;
    /**
     * fixture: the sum and product of the first two columns as obtained via a
     * filter
     */
    private final SumAndProduct sumAndProduct;
    /**
     * fixture: the suspected sum and product form the csv file.
     */
    private final int suspectedSumAndProduct;

    /**
     *
     * @return a list of fixtures build f the csv file. the first two filters
     *         are default filters from the system, the next creates the sum and
     *         product, and the 4th reads the 3rd column from the file.
     * @throws IOException
     *             if the csv file could not be read
     */
    @Parameters(name = "{index}: [{0},{1}]->{2}")
    public static List<Object[]> data() throws IOException {
        return CSVReader.getData(ProductsAndSumsDemo.class.getResource(SUM_AND_PRODUCT_CSV),
                "\\t+", new IntegerFilter(1), new IntegerFilter(2), SumAndProductFilter.INSTANCE,
                new ErrorIgnoringIntegerFilter(3));
    }

    /**
     * setup the fixture
     *
     * @param a
     * @param b
     * @param sumOrProduct
     * @param suspectedSumAndProduct
     */
    public ProductsAndSumsDemo(int a, int b, SumAndProduct sumOrProduct, int suspectedSumAndProduct) {
        this.valueA = a;
        this.valueB = b;
        this.sumAndProduct = sumOrProduct;
        this.suspectedSumAndProduct = suspectedSumAndProduct;
    }

    /**
     * Test whether the third column is the product of the first two.
     *
     * This fails in some cases on intent!
     */
    @Test
    public void testIsProduct() {
        assertThat(String.format("%d is not a the product of %d and %d", suspectedSumAndProduct,
                valueA, valueB), suspectedSumAndProduct, equalTo(sumAndProduct.getProduct()));
    }

    /**
     * Test whether the third column is the sum of the first two.
     *
     * This fails in some cases on intent!
     */
    @Test
    public void testIsSum() {
        assertThat(String.format("%d is not a the sum of %d and %d", suspectedSumAndProduct,
                valueA, valueB), suspectedSumAndProduct, equalTo(sumAndProduct.getSum()));
    }

}
