/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.demo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.CSVReader;
import org.bitbucket.gthimm.junitextras.csv.CSVReaderGetDataTestBase;
import org.bitbucket.gthimm.junitextras.csv.filters.DoubleFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.IntegerFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Example for the integration of the CSV file reader in a parametrized test
 *
 */
@RunWith(Parameterized.class)
public class CSVReaderDemo {

    private static final Logger LOG = LoggerFactory.getLogger(CSVReaderDemo.class);

    /**
     * fixture: the value in the 4th column of the csv file
     */
    private final double forthColumn;
    /**
     * fixture: the first column from the csv file
     */
    private final int firstColumn;
    /**
     * fixture: the 2nd column from the csv file
     */
    private final String secondColumn;

    /**
     *
     * @return a list of fixtures build from the 2nd, 1st, and 4th column of the
     *         csv file. The values are translated into a string, Integer, and
     *         Double, respectively
     */
    @Parameters
    public static List<Object[]> data() throws IOException {
        return CSVReader.getData(
                CSVReaderGetDataTestBase.class.getResource("test2.csv"), ",",
                new StringFilter(2), new IntegerFilter(1), new DoubleFilter(4));
    }

    /**
     * The constructor takes the arguments as extracted in <code>data()</code>
     * and writes them into the actual fixture-fileds of the test
     *
     * @param s
     * @param i
     * @param d
     */
    public CSVReaderDemo(String s, Integer i, Double d) {
        this.secondColumn = s;
        this.firstColumn = i;
        this.forthColumn = d;
    }

    /**
     * test the data. test2.csv contains:
     *
     * <pre>
     * 	  1,"A","one",1.1
     * 	  2,"B","two",2.2
     * 	  3,"C","three",3.3
     * </pre>
     */
    @Test
    public void test() {
        switch (firstColumn) {
        case 1:
            assertThat(secondColumn, equalTo("A"));
            assertThat(forthColumn, equalTo(1.1));
            break;
        case 2:
            assertThat(secondColumn, equalTo("B"));
            assertThat(forthColumn, equalTo(2.2));
            break;
        default:
            LOG.warn("skipping test for i={}", firstColumn);
        }
    }
}
