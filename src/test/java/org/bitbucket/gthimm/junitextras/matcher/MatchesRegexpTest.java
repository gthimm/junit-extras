/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.matcher;

import static org.bitbucket.gthimm.junitextras.matcher.ExtrasMatcher.containsRegex;
import static org.bitbucket.gthimm.junitextras.matcher.ExtrasMatcher.matchesRegex;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * test the test: the regexp matcher
 */

public class MatchesRegexpTest {
    /**
     * test that the test recognises that 2 is at least one
     **/
    @Test
    public void testMatches() throws Exception {
        assertThat("aaa", matchesRegex("a{3}"));
    }

    /**
     * test that the test recognises that 2 is at least one
     **/
    @Test
    public void testContains() throws Exception {
        assertThat("_aaab", containsRegex("a{3}"));
    }

    /**
     * test that the test recognises that 2 is at least one
     **/
    @Test(expected = AssertionError.class)
    public void testContainsFail() throws Exception {
        assertThat("_aab", containsRegex("a{3}"));
    }

    /**
     * test that the test recognises that one is at least one
     **/
    @Test(expected = AssertionError.class)
    public void testDoesNotMatch() throws Exception {
        assertThat("b", matchesRegex("c"));
    }
}
