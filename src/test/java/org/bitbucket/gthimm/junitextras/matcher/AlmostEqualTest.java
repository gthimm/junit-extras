/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */
package org.bitbucket.gthimm.junitextras.matcher;

import static org.bitbucket.gthimm.junitextras.matcher.ExtrasMatcher.isAlmostEqual;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

public class AlmostEqualTest {

    /**
     * test that for some examples the isAlmostEqual-matcher passes
     **/
    @Test
    public void testPasses() {

        // values are integers, tolerance a double
        assertThat(1, isAlmostEqual(1, 0.1));
        assertThat(1, not(isAlmostEqual(2, 0.999)));
        assertThat(1, not(isAlmostEqual(0, 0.999)));
        assertThat(1, isAlmostEqual(2, 1.0));
    }

    @Test
    public void doubleWithIntegerInterval() {
        assertThat(-3.5, isAlmostEqual(-3, 1));
    }

    @Test
    public void doubleWithIntegerTolerance() {
        assertThat(9110.2, isAlmostEqual(9108.8, 3));
    }

    @Test
    public void doubleIdentity() { // test for doubles
        assertThat(1d, isAlmostEqual(1d, 0.0));

    }

    @Test
    public void integerIdentity() {
        assertThat(1, isAlmostEqual(1, 0));
    }

    @Test
    public void integerOnRightInterval() {
        assertThat(1, isAlmostEqual(-1, 2));
    }

    @Test
    public void integerOnLeftInterval() {
        assertThat(-3, isAlmostEqual(-1, 2));
    }

    /**
     * test that the assertion fails if the tolerance is insufficient wide
     **/
    @Test(expected = AssertionError.class)
    public void integerIsNotAlmost2() {
        assertThat(1, isAlmostEqual(2, 0));
    }

    @Test(expected = AssertionError.class)
    public void doubleIsBelowInterval() {
        assertThat(1.1, isAlmostEqual(1.2, 0.05));
    }

    @Test(expected = AssertionError.class)
    public void doubleIsAboveInterval() {
        assertThat(1.1, isAlmostEqual(1.0, 0.05));
    }

    @Test(expected = AssertionError.class)
    public void testIntergerBelowInterval() {
        assertThat(100L, isAlmostEqual(110, 9));
    }

    @Test(expected = AssertionError.class)
    public void testBelowIntervalWithDoubleTolerance() {
        assertThat(100L, isAlmostEqual(110, 9.1));
    }

    /**
     * test that NaN is handled and an assertion error is thrown in all cases
     */
    @Test(expected = AssertionError.class)
    public void testForRefValueIsNaN() {
        assertThat("NaN as ref value", Double.NaN, isAlmostEqual(0.0, 1.0));
    }

    @Test(expected = AssertionError.class)
    public void testForTestValueIsNaN() {
        assertThat("NaN as Test value", 0.0, isAlmostEqual(Double.NaN, 1.0));
    }

    @Test(expected = AssertionError.class)
    public void testForToleranceValueIsNaN() {
        assertThat("NaN as Tolerance value", 0.0, isAlmostEqual(0.0, Double.NaN));
    }

    /**
     * test that Infinity is handled and an assertion error is thrown in all
     * cases
     */
    @Test(expected = AssertionError.class)
    public void testForRefValueIsInfinity() {
        assertThat("NaN as ref value", Double.NEGATIVE_INFINITY, isAlmostEqual(0.0, 1.0));
    }

    @Test(expected = AssertionError.class)
    public void testForTestValueIsInfinity() {
        assertThat("NaN as Test value", 0.0, isAlmostEqual(Double.NEGATIVE_INFINITY, 1.0));
    }

    @Test(expected = AssertionError.class)
    public void testForToleranceValueIsInfinity() {
        assertThat("NaN as Tolerance value", 0.0, isAlmostEqual(0.0, Double.NEGATIVE_INFINITY));
    }

    /**
     * test {@link AtomicInteger}
     */
    @Test
    public void testAtomicInteger() {
        AtomicInteger zero = new AtomicInteger();
        AtomicInteger a = new AtomicInteger();
        a.addAndGet(1);
        assertThat(a, isAlmostEqual(zero, 1));
        a.addAndGet(5);
        assertThat(a, not(isAlmostEqual(zero, 1)));
    }

    /**
     * BigIntegers are not handled by the test - therefore an
     * {@link AssertionError} is thrown.
     */
    @Test(expected = AssertionError.class)
    public void testBigInteger() {
        assertThat(new BigInteger("1111111"), isAlmostEqual(new BigInteger("1111111"), 1));
    }

    /**
     * ensure that if the tolerance is negative, an {@link AssertionError} is
     * thrown
     */
    @Test(expected = AssertionError.class)
    public void testToleranceNotNegative() {
        isAlmostEqual(1, -1);
    }

    /**
     * the matcher may only be build if the parameters to isAlmostEqual() are of
     * certain types
     */
    @Test
    public void testRejectInvalidArgumentTypes() {
        final String error = "isAlmostEqual should not accept ";
        // check invalid types
        for (Number n : new Number[] { BigInteger.ONE, BigDecimal.ONE }) {
            // check the first argument
            try {
                isAlmostEqual(n, 0);
                fail(error + n.getClass().getCanonicalName() + " as first parameter");
            } catch (AssertionError e) {
                if (e.getMessage().startsWith(error)) {
                    throw new AssertionError(e);
                    // else ignore - this is the right behaviour
                }
            }
            // check the second argument
            try {
                isAlmostEqual(0, n);
                fail(error + n.getClass().getCanonicalName() + " as second parameter");
            } catch (AssertionError e) {
                if (e.getMessage().startsWith(error)) {
                    throw new AssertionError(e);
                    // else ignore - this is the right behaviour
                }
            }
            // check matches() the test via an assertion
            try {
                assertThat(n, isAlmostEqual(1, 1));
                fail(error + n.getClass().getCanonicalName() + " in the matcher");
            } catch (AssertionError e) {
                if (e.getMessage().startsWith(error)) {
                    throw new AssertionError(e);
                    // else ignore - this is the right behaviour
                }
            }
        }
    }
}
