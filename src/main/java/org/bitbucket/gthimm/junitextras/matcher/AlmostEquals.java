/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.matcher;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Compare numbers for equality while allowing a given tolerance. Arguments may
 * be formally a specialisation of {@link java.lang.Number}, but the matcher is
 * only guaranteed to work for {@link Double}, {@link Float}, {@link Integer},
 * {@link Short}, {@link Byte} (and the primitive equivalents),
 * {@link AtomicLong} and {@link AtomicInteger}. If any of the value to be
 * matched, the reference value or the tolerance is an instance of
 * {@code java.lang.Double} or {@code java.lang.Float}, the matcher uses the
 * {@link Number#doubleValue()}, otherwise the {@link Number#longValue()}.
 */
public class AlmostEquals extends BaseMatcher<Number> {

    private static final Logger LOG = LoggerFactory.getLogger(AlmostEquals.class);

    private final Number number;
    private final Number tolerance;

    /**
     *
     * @param number
     *            the should value for the number.
     * @param tolerance
     *            the maximal difference between number and the tested value
     * @exception AssertionError
     *                if the tolerance is negative or the types of number and
     *                tolerance cannot be handled.
     */
    public AlmostEquals(final Number number, final Number tolerance) {
        typeIsValid(number);
        typeIsValid(tolerance);
        if (tolerance.doubleValue() < 0) {
            throw new AssertionError(
                    String.format("tolerance=%s is negative", tolerance.toString()));
        }
        this.number = number;
        this.tolerance = tolerance;
    }

    /**
     * Check whether {@code number} is of a type this matcher can deal with
     *
     * @param number
     *            the number to be examined
     * @exception AssertionError
     *                if the type cannot be handled.
     */
    private static void typeIsValid(Number number) {
        if (!(number instanceof Double || number instanceof Float || number instanceof Long
                || number instanceof Integer || number instanceof Byte || number instanceof Short
                || number instanceof AtomicInteger || number instanceof AtomicLong)) {
            throw new AssertionError("type " + number.getClass().getCanonicalName()
                    + " cannot be handled");
        }
    }

    public void describeTo(Description description) {
        description.appendValue("expected a number that is within a +/-" + tolerance
                + " interval from " + number + ". ");
    }

    /**
     * check arg0 to be with in the range +- {@link #tolerance} of
     * {@link #number}. for simplicity, if one of arg0, number or tolerance is a
     * floating point number, the comparison is done on the base of doubles;
     * otherwise as longs.
     *
     * @exception AssertionError
     *                if this is not the case of the type of arg0 cannot be
     *                dealt with.
     */
    public boolean matches(Object numberObject) {
        final Number value = (Number) numberObject;
        typeIsValid(value);
        if (number instanceof Double || number instanceof Float || tolerance instanceof Double
                || tolerance instanceof Float || value instanceof Double || value instanceof Float) {
            LOG.info("matching {}({}) as double", value, value.getClass());
            return value.doubleValue() <= number.doubleValue() + tolerance.doubleValue()
                    && value.doubleValue() >= number.doubleValue() - tolerance.doubleValue();
        }
        LOG.info("matching {}({}) as long", value, value.getClass());
        return value.longValue() <= number.longValue() + tolerance.longValue()
                && value.longValue() >= number.longValue() - tolerance.longValue();
    }
}
