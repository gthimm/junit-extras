/**
 * This file is part of the junit-extras and is covered by the GNU Library or
 * "Lesser" General Public License version 3.0 (LGPLv3)
 */
package org.bitbucket.gthimm.junitextras.matcher;

/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

import org.hamcrest.Factory;
import org.hamcrest.Matcher;

/**
 * Factories for matchers provided by junit-extras
 *
 */
public final class ExtrasMatcher {

    private ExtrasMatcher() {
        // this class should not be instantiated
    }

    /**
     *
     * @param regex
     *            a java-style regular expression
     * @return a matcher of type {@link MatchesRegexp} where the whole matched
     *         string must match {@code regex}.
     */
    @Factory
    public static MatchesRegexp matchesRegex(String regex) {
        return new MatchesRegexp(regex, true);
    }

    /**
     * @param regex
     *            a java-style regular expression
     * @return a matcher of type {@link MatchesRegexp} for which a partial match
     *         results into a success.
     */
    @Factory
    public static MatchesRegexp containsRegex(String regex) {
        return new MatchesRegexp(regex, false);
    }

    /**
     * @param value
     *            the value to which a given number should be similar
     * @param tolerance
     *            the tolerance that should be applied
     * @return a matcher testing whether a value is within a symmetric interval
     *         (including the limits) around {@code value}
     *
     */
    @Factory
    public static Matcher<Number> isAlmostEqual(Number value, Number tolerance) {
        return new AlmostEquals(value, tolerance);
    }
}
