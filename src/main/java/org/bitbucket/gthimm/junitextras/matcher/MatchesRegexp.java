/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.matcher;

/**
 * a matcher permitting to match a regular expression against a string or the
 * value of the "toString()" method
 */
import java.util.regex.Pattern;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class MatchesRegexp extends BaseMatcher<String> {

    private final Pattern regexp;
    private final boolean fullMatchRequired;

    /**
     *
     * @param regex
     *            the regular expression
     * @param fullMatchRequired
     *            if true, the entire string must match match the regex, if
     *            falls, a partial match is sufficient.
     */
    public MatchesRegexp(String regex, boolean fullMatchRequired) {
        this.regexp = Pattern.compile(regex);
        this.fullMatchRequired = fullMatchRequired;
    }

    public boolean matches(String input) {
        if (fullMatchRequired) {
            return regexp.matcher(input).matches();
        }
        return regexp.matcher(input).find();
    }

    public void describeTo(Description description) {
        description.appendText("a string or an object where toString() matches the regexp <");
        description.appendText(regexp.pattern());
        description.appendText(">");
    }

    public boolean matches(Object string) {
        if (string == null) {
            return false;
        }
        return matches(string.toString());
    }
}
