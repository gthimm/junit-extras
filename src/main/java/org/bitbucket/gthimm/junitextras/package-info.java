package org.bitbucket.gthimm.junitextras;

/**
 * The junit extras are in three packages:
 *
 * <b>suite</b>, which in addition to the list of classes in the "official"
 * suite implementation allows to collect test classes via regular expressions
 * matched against qualified class names.
 *
 * <b>csv</b> provides classes for reading CSV-files and return objects of type
 * List\<Object\[\]\>, ideal for the creation of parametrised test classes.
 * Filters are provided for String, Double, Integer, Long, Boolean, and Dates
 *
 * <b>matcher</b> includes additional matchers to work with assertThat().
 */
