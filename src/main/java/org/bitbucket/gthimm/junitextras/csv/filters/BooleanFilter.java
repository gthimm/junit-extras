/* *********************************************************************
 * 
 * Copyright [2015] [Georg Thimm]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convert a column into a boolean value. Boolean "strings" are : "true",
 * "false", "t", "f" (upper/lower case is ignored). the string "ignore" is
 * encountered, the exception {@link CSVIgnoreRow} is thrown. Otherwise,
 * {@link InvalidCSVFormatException} is thrown.
 *
 */
public class BooleanFilter extends ColumnFilterImpl<Boolean> {
    private static final Logger LOG = LoggerFactory.getLogger(BooleanFilter.class);

    /**
     * @param column
     *            the column that is to be converted into a boolean value
     */
    public BooleanFilter(final int column) {
        super(column);
    }

    /**
     * @param column
     *            the column to be filtered
     * @param enableIgnore
     *            see {@link ColumnFilterImpl#ColumnFilterImpl(int, boolean)}
     */
    public BooleanFilter(final int column, final boolean enableIgnore) {
        super(column, enableIgnore);
    }

    /**
     * @param row
     *            see {@link ColumnFilter#apply(List)}
     */
    @Override
    public final Boolean apply(final List<String> row) throws CSVIgnoreRow {
        String value = getColumn(row);
        if (value.compareToIgnoreCase("true") == 0 || value.compareToIgnoreCase("t") == 0) {
            return Boolean.TRUE;
        }
        if (value.compareToIgnoreCase("false") == 0 || value.compareToIgnoreCase("f") == 0) {
            return Boolean.FALSE;
        }
        throw new InvalidCSVFormatException("Cannot convert " + value + " to boolean.");
    }
}
