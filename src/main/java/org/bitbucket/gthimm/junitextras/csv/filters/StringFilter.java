/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

/**
 * Used in combination with {@code CSVReader.getData()}
 *
 */
public class StringFilter extends ColumnFilterImpl<String> {

    /**
     * @param column
     *            the column from which to extract a {@code String }
     * @see ColumnFilterImpl
     * */
    public StringFilter(final int column) {
        super(column);
    }

    /**
     * @param column
     *            the column from which to extract a {@code String }
     * @param enableIgnore
     *            see {@link ColumnFilterImpl}
     */
    public StringFilter(final int column, final boolean enableIgnore) {
        super(column, enableIgnore);
    }

    /**
     * @param row
     *            a row of data from a test data file
     * @return the data in the specified column of the row.
     * @throws CSVIgnoreRow
     *             if the row should be ignored.
     * @throws InvalidCSVFormatException
     *             if the specified column does not exist
     */
    @Override
    public String apply(List<String> row) throws CSVIgnoreRow {
        return getColumn(row);
    }
}
