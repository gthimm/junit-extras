/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

public abstract class NumberFilter<T extends Number> extends ColumnFilterImpl<T> {
    /**
     * @param column
     *            the column from which to extract a {@code Number}
     * @see ColumnFilterImpl#ColumnFilterImpl(int)
     */
    public NumberFilter(int column) {
        super(column);
    }

    /**
     * @param column
     *            the column from which to extract a {@code Number}
     * @param enableIgnore
     *            see {@link ColumnFilterImpl}
     * @see ColumnFilterImpl#ColumnFilterImpl(int, boolean)
     */
    public NumberFilter(int column, final boolean enableIgnore) {
        super(column, enableIgnore);
    }

    @Override
    public T apply(List<String> row) throws CSVIgnoreRow {
        String value = getColumn(row);
        try {
            return valueOf(value);
        } catch (NumberFormatException e) {
            throw new InvalidCSVFormatException("Cannot convert  " + value + "to double.");
        }
    }

    /**
     *
     * @param value
     *            the String to be converted to Type T
     * @return a number.
     */
    abstract T valueOf(String value);
}
