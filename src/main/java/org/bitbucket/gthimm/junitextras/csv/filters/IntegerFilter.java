/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

/**
 * a filter for Integer numbers. Absence of a column or a non-integer value
 * results into a {@link InvalidCSVFormatException}.
 *
 */
public class IntegerFilter extends NumberFilter<Integer> {

    /**
     * @param column
     *            the column from which to extract an {@code Integer}
     * @see ColumnFilterImpl
     */
    public IntegerFilter(int column) {
        super(column);
    }

    /**
     * @param column
     *            the column from which to extract an {@code Integer}
     *
     * @param enableIgnore
     *            see {@link ColumnFilterImpl}
     */
    public IntegerFilter(int column, boolean enableIgnore) {
        super(column, enableIgnore);
    }

    @Override
    Integer valueOf(String value) {
        return Integer.parseInt(value);
    }
}
