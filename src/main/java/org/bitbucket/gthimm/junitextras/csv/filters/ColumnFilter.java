/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

/**
 * This interface defines the method apply, which is meant to convert a row of
 * data into an Entry in the test fixture. Typically - but not necessarily -
 * this would translate some column in a specific value.
 *
 */
public interface ColumnFilter<T> {

    /**
     * @param row
     *            a row of data from which an object is to be generated
     * @return an object extracted from this row.
     * @throws CSVIgnoreRow
     *             when a row should be ignored
     * @throws InvalidCSVFormatException
     *             if no data could be found or the data could not be parsed.
     */
    T apply(List<String> row) throws CSVIgnoreRow, InvalidCSVFormatException;
}
