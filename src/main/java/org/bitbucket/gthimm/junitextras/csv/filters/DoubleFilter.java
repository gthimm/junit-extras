/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

public class DoubleFilter extends NumberFilter<Double> {
    /**
     * @param column
     *            the column from which to extract a {@code Double}
     * @see ColumnFilterImpl
     */
    public DoubleFilter(final int column) {
        super(column);
    }

    /**
     * @param column
     *            the column from which to extract a value.
     * @param enableIgnore
     *            see {@link ColumnFilterImpl#ColumnFilterImpl(int, boolean)}
     */
    public DoubleFilter(final int column, final boolean enableIgnore) {
        super(column, enableIgnore);
    }

    @Override
    Double valueOf(String value) {
        return Double.valueOf(value);
    }
}
