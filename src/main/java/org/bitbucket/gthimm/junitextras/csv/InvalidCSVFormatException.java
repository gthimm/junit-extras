/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

/**
 * This exception is thrown if the CSV file is inconsistent or unsuitable for
 * the generation of parameterised tests (i.e. the translation of a string found
 * in a given column can not be translated into the right data type).
 *
 */
public class InvalidCSVFormatException extends Error {

    private static final long serialVersionUID = 1L;

    /**
     * @param error
     *            an explanation why the translation is not possible
     */
    public InvalidCSVFormatException(String error) {
        super(error);
    }

}
