/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

/**
 * To be thrown by a ColumnFilter if a row in a csv file is to be ignored. This
 * does NOT signal a problem in the input but the intend of the user not to
 * translate a certain row in the CSV file into a test.
 */
@SuppressWarnings("serial")
public class CSVIgnoreRow extends Exception {
    // nothing needed here...
}
