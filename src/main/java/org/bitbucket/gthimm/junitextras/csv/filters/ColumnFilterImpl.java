/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

/**
 * Convenience implementation for filter extraction values from just one column
 *
 */
public abstract class ColumnFilterImpl<T> implements ColumnFilter<T> {

    /**
     * If a value in the CSV equals this tag and either an instance of this
     * class was instantiated using {@link #ColumnFilterImpl(int)} or
     * {@link #ColumnFilterImpl(int, boolean)} with {@code enableIgnore=true}, a
     * row is ignored.
     */
    public static final String IGNORE_TAG = "ignore";

    private final int column;

    private final boolean enableIgnore;

    /**
     * A filter for the <code>column</code>. Rows where this column contains the
     * string "ignore" cause {@link CSVIgnoreRow} to be thrown.
     *
     * @param column
     *            the column from which the data should be taken (left most
     *            column is column 1).
     */
    public ColumnFilterImpl(int column) {
        this.column = column - 1;
        enableIgnore = true;
    }

    /**
     * A filter for the <code>column</code>.
     *
     * @param column
     *            the column from which the data should be taken (left most
     *            column is column 1).
     * @param enableIgnore
     *            Rows where column contains the string "ignore" cause
     *            {@link CSVIgnoreRow} to be thrown and
     *            {@link InvalidCSVFormatException} otherwise.
     */
    public ColumnFilterImpl(int column, boolean enableIgnore) {
        this.column = column - 1;
        this.enableIgnore = enableIgnore;
    }

    /**
     * The specialising filter should use this method or the string "ignore" may
     * not be handled correctly
     *
     * @param row
     *            the row number from which the data was obtained. Used for
     *            meaningful error description.
     * @return the column's element in row.
     * @throws CSVIgnoreRow
     *             if enableIgnore is true and the column contains the string
     *             "ignore" (case ignored).
     * @exception InvalidCSVFormatException
     *                if the row does not have enough elements.
     */
    protected final String getColumn(final List<String> row) throws CSVIgnoreRow {
        try {
            String value = row.get(column);
            if (enableIgnore && value.equalsIgnoreCase(IGNORE_TAG)) {
                throw new CSVIgnoreRow();
            }
            return value;
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidCSVFormatException("No element " + column + " in " + row);
        }
    }

    public abstract T apply(List<String> row) throws CSVIgnoreRow;

}
