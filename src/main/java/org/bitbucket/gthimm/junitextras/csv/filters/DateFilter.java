/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv.filters;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.InvalidCSVFormatException;

public class DateFilter extends ColumnFilterImpl<Date> {

    private final DateFormat dateFormat;

    /**
     * @param column
     *            the column from which to extract a {@code Date}
     * @param dateFormat
     *            see {@link DateFilter#DateFilter(int, DateFormat, boolean)}
     */
    public DateFilter(final int column, final DateFormat dateFormat) {
        super(column);
        this.dateFormat = dateFormat;
    }

    /**
     *
     * @param column
     *            the column from which the date should be obtained
     * @param dateFormat
     *            the date format to be used to parse the date. Note that if
     *            dateFormat is locale-dependent, the locale likely needs to be
     *            set before the construction of dateFormat.
     * @param enableIgnore
     *            see {@link ColumnFilterImpl#ColumnFilterImpl(int, boolean)}
     */
    public DateFilter(final int column, final DateFormat dateFormat, final boolean enableIgnore) {
        super(column, enableIgnore);
        this.dateFormat = dateFormat;
    }

    /**
     * @see ColumnFilterImpl
     */
    @Override
    public Date apply(List<String> row) throws CSVIgnoreRow {
        String value = getColumn(row);
        Date date = null;
        try {
            date = dateFormat.parse(value);
        } catch (ParseException e) {
            throw new InvalidCSVFormatException("Could not parse " + value + " as date");
        }
        return date;
    }

}
