/* *********************************************************************
 *
 * Copyright [2015] [Georg Thimm]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * **********************************************************************
 */

package org.bitbucket.gthimm.junitextras.csv;

import static org.junit.Assert.fail;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.gthimm.junitextras.csv.filters.CSVIgnoreRow;
import org.bitbucket.gthimm.junitextras.csv.filters.ColumnFilter;
import org.bitbucket.gthimm.junitextras.csv.filters.StringFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code getData()} methods in this class are intended as support for
 * parameterised JUnit tests.
 *
 * <h2>Example</h2> Suppose that the file testdata.csv is located along with
 * ExampleTest.class, and it contains the data
 *
 * <pre>
 * 1,"A","one",1.1
 * 2,"B","two",2.2
 * 3,"C","three",3.3
 * </pre>
 *
 * then the test could look like
 *
 * <pre>
 * &#064;RunWith(Parameterized.class)
 * public class ExampleTest {
 *
 *     private double d;
 *     private int i;
 *     private String s;
 *
 *     &#064;Parameters
 *     public static List&lt;Object[]&gt; data() {
 *         return CSVReader.getData(CSVReaderExampleTest.class.getResource(&quot;testdata.csv&quot;), &quot;,&quot;, 2, 1,
 *                 4);
 *     }
 *
 *     public CSVReaderExampleTest(String s, String i, String d) {
 *         this.s = s;
 *         this.i = Integer.parseInt(i);
 *         this.d = Double.parseDouble(d);
 *     }
 *
 *     &#064;Test
 * 	public void test() {
 *      ....
 *      }
 * }
 * </pre>
 */
public final class CSVReader {

    private static final Logger LOG = LoggerFactory.getLogger(CSVReader.class);

    private static Charset usedFileEncoding = Charset.defaultCharset();

    private CSVReader() {
        // this class should not be instantiated
    }

    /**
     * Read the data and return a list of String-arrays for a junit parametrised
     * test.
     *
     * @param url
     *            the source for the data file (only files are supported).
     * @param separator
     *            the separator for items in one line of the CSV file.
     * @return data suitable for the creation of a junit parametrised test
     */
    @SuppressWarnings("resource")
    public static List<Object[]> getData(URL url, String separator) {
        List<Object[]> list = new ArrayList<Object[]>();
        FileInputStream inputStream = null;
        InputStreamReader inputReader = null;
        LineNumberReader lnr = null;
        try {
            inputStream = new FileInputStream(new File(url.toURI()));
            inputReader = new InputStreamReader(inputStream, usedFileEncoding);
            lnr = new LineNumberReader(inputReader);
            do {
                String line = lnr.readLine();
                if (line != null) {
                    line = line.trim();
                    if (!line.isEmpty()) {
                        list.add(splitLineToStringArray(line, separator));
                    }
                } else {
                    break;
                }
            } while (true);
        } catch (Exception e) {
            fail("could not read test data from " + url + ": " + e.getClass() + " "
                    + e.getMessage());
        } finally {
            forceClose(lnr);
            forceClose(inputReader);
            forceClose(inputStream);
        }
        return list;
    }

    /**
     * @param line
     *            the string to be split and columns taken from.
     * @param separator
     *            the regular expression which separates two tokens in a line.
     * @param columns
     *            the columns of interest
     * @return a string array with the columns as in parameter columns.
     */
    static String[] splitLineToStringArray(String line, String separator, int... columns) {
        List<String> list = splitLineToListOfStrings(line, separator);
        if (columns.length == 0) {
            return list.toArray(new String[list.size()]);
        }
        String[] array = new String[columns.length];
        for (int col = 0; col < columns.length; col++) {
            int index = columns[col] - 1;
            if (index < 0 || index >= list.size()) {
                throw new InvalidCSVFormatException("No such column: " + columns[col]);
            }
            array[col] = list.get(index);
        }
        return array;
    }

    /**
     * Add the next token from the input to tokens and call this recursively.
     *
     * @param input
     *            the (remainder of) a line to be parsed
     * @param tokens
     *            the tokens already parsed
     * @param separator
     *            the regular expression which separates two tokens in a line.
     */
    private static void nextToken(String input, List<String> tokens, String separator) {
        if (input.length() == 0) {
            tokens.add(input);
        } else {
            if (input.charAt(0) == '"') {
                int pos2 = nextDoubleQuote(input, 1);
                if (pos2 == 1) {
                    tokens.add("\"");
                } else {
                    String left = input.substring(1, pos2);
                    tokens.add(left);
                }
                String[] rest = input.substring(pos2 + 1).split("\\s*" + separator + "\\s*", 2);
                if (rest[0].length() > 0) {
                    throw new InvalidCSVFormatException("found illegal format in input: " + input);
                }
                if (rest.length > 1) {
                    nextToken(rest[1], tokens, separator);
                }
            } else {
                String[] result = input.split("\\s*" + separator + "\\s*", 2);
                tokens.add(result[0].trim());
                if (result.length > 1) {
                    nextToken(result[1], tokens, separator);
                }
            }
        }
    }

    /**
     *
     * @param input
     *            The string in which to search for a double quote
     * @param startPos
     *            the starting position
     * @return the first index of a double quote (a quote left of the starting
     *         position is ignored). As the CSV file format uses double-double
     *         quotes to encode double quotes, these must be ignored.
     * @exception InvalidCSVFormatException
     *                if no double quote is found.
     */
    static int nextDoubleQuote(final String input, final int startPos) {
        int pos = startPos;
        do {
            pos = input.indexOf('"', pos);
            if (pos < 0) {
                throw new InvalidCSVFormatException("found illegal format in input: " + input);
            }
            if (pos + 1 < input.length() && input.charAt(pos + 1) == '"') {
                pos += 2;
                if (pos >= input.length()) {
                    throw new InvalidCSVFormatException("found illegal format in input: " + input);
                }
            } else {
                return pos;
            }
        } while (true);
    }

    /**
     * As {@link #getData(URL, String)} with the default separator.
     *
     * @param url
     *            the source for the data file (only files are supported).
     * @return a list of data-arrays
     */
    public static List<Object[]> getData(final URL url) {
        return getData(url, ",");
    }

    /**
     * As {@link #getData(URL, String)} with the default separator returning all
     * columns.
     *
     * @param url
     *            the source for the data file (only files are supported).
     * @param columns
     *            the columns to be extracted.
     * @return a list of data-arrays
     * @throws IOException
     *             if the file corresponding to {@code url} could not be read.
     */
    public static List<Object[]> getData(final URL url, final int... columns) throws IOException {
        return getData(url, ",", columns);
    }

    /**
     * read the data and return a list of String-arrays.
     *
     * @param url
     *            the source for the data file (only files are supported).
     * @param separator
     *            the separator used to split lines.
     * @param columns
     *            the columns to be extracted (the leftmost column in the file
     *            has the index 1).
     * @return a list of arrays.
     * @throws IOException
     *             if the file corresponding to {@code url} could not be read.
     * @exception InvalidCSVFormatException
     *                if the file could not be parsed.
     */
    public static List<Object[]> getData(final URL url, final String separator,
            final int... columns) throws IOException {
        ColumnFilter[] filters = new StringFilter[columns.length];
        for (int i = 0; i < columns.length; i++) {
            filters[i] = new StringFilter(columns[i]);
        }
        return getData(url, separator, filters);
    }

    /**
     * Split a string with a comma as separator.
     *
     * @param url
     *            see {@link #getData(URL, String, boolean, ColumnFilter...)}
     * @param columnFilter
     *            see {@link #getData(URL, String, boolean, ColumnFilter...)}
     * @return same as {@link #getData(URL, String, boolean, ColumnFilter...)}
     *
     * @throws IOException
     *             if the file corresponding to {@code url} could not be read.
     */
    public static List<Object[]> getData(URL url, ColumnFilter... columnFilter) throws IOException {
        return getData(url, ",", columnFilter);
    }

    /**
     * @param url
     *            the origin of the data to be filtered
     * @param separator
     *            see {@link #getData(URL, String, boolean, ColumnFilter...)}
     * @param columnFilter
     *            see {@link #getData(URL, String, boolean, ColumnFilter...)}
     * @return see {@link #getData(URL, String, boolean, ColumnFilter...)}
     * @throws IOException
     *             if the file corresponding to {@code url} could not be read.
     */
    public static List<Object[]> getData(URL url, String separator, ColumnFilter... columnFilter)
            throws IOException {
        return getData(url, separator, false, columnFilter);
    }

    /**
     * @param url
     *            the origin of the data to be filtered
     * @param separator
     *            A string which separates columns (e.g. ";")
     * @param skipHeader
     *            whether of not the first line of the file should be ignored.
     * @param columnFilters
     *            a set of filters.
     * @return a list of Object arrays obtained from the data file.
     * @throws IOException
     *             If the file corresponding to {@code url} could not be opened.
     */
    public static List<Object[]> getData(URL url, String separator, boolean skipHeader,
            ColumnFilter... columnFilters) throws IOException {
        final List<Object[]> list = new ArrayList<Object[]>();
        FileInputStream inputStream = null;
        InputStreamReader inputReader = null;
        LineNumberReader lnr = null;
        try {
            inputStream = new FileInputStream(new File(url.toURI()));
            inputReader = new InputStreamReader(inputStream, usedFileEncoding);
            lnr = new LineNumberReader(inputReader);

            String line = null;
            if (skipHeader) {
                lnr.readLine();
            }
            do {
                try {
                    line = lnr.readLine();
                    if (line != null) {
                        line = line.trim();
                        if (!line.isEmpty()) {
                            // row is the array resulting in a fixture
                            List<Object> row = new ArrayList<Object>();
                            // rowStrings are the strings from a line in the csv
                            // file
                            List<String> rowStrings = splitLineToListOfStrings(line, separator);
                            // apply the filters and collect the result.
                            for (ColumnFilter columnFilter : columnFilters) {
                                row.add(columnFilter.apply(rowStrings));
                            }
                            list.add(row.toArray());
                        }
                    } else {
                        break;
                    }
                } catch (CSVIgnoreRow e) {
                    LOG.warn("ignoring line " + lnr.getLineNumber() + ": " + line);
                }
            } while (true);

        } catch (URISyntaxException e) {
            LOG.warn("could not open file with URL={}: {} {}", url, e.getClass(), e.getMessage());
        } finally {
            forceClose(lnr);
            forceClose(inputReader);
            forceClose(inputStream);
        }
        return list;
    }

    /**
     * @param line
     *            The line to be split into an array.
     * @param separator
     *            a regular expresion describing where to separate the
     *            {@code line}.
     * @return strings as resulting from splitting a line of text.
     */
    @SuppressWarnings("serial")
    private static List<String> splitLineToListOfStrings(final String line, final String separator) {
        List<String> list = new ArrayList<String>() {
            @Override
            public boolean add(String arg0) {
                return super.add(normalise(arg0));
            }

            /**
             * replace CVSisms by the corresponding characters.
             *
             * @param token
             * @return the normalized token
             */
            private String normalise(String token) {
                return token.replaceAll("\"\"", "\"");
            }
        };
        nextToken(line, list, separator);
        return list;
    }

    /**
     * @param charSet
     *            the char set to be usede while reading files.
     */
    public static void setUsedFileEncoding(Charset charSet) {
        usedFileEncoding = charSet;
    }

    private static void forceClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                LOG.warn("could not close : {} {}", closeable.getClass(), e.getClass(),
                        e.getMessage());
            }
        }
    }
}
